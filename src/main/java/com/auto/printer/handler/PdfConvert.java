package com.auto.printer.handler;

import com.auto.printer.entity.dto.PdfConvertDTO;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author LoveHuaHua
 * @date 2023年08月01日 8:28
 * @description believe in yourself
 */
@Component
public class PdfConvert implements IPdfConvert{
    /**
     * 转换
     *
     * @param fileUrl 文件地址
     * @param bytes   文件字节
     * @return {@link String}
     */
    @Override
    public PdfConvertDTO convert(String fileUrl, byte[] bytes) throws IOException {
        PDDocument load = PDDocument.load(bytes);
        return new PdfConvertDTO(load,fileUrl);
    }

    /**
     * 是否支持
     *
     * @param fileSuffix 文件后缀
     * @return boolean
     */
    @Override
    public boolean isSupport(String fileSuffix) {
        return "pdf".equalsIgnoreCase(fileSuffix);
    }

    /**
     * 排序 越大优先级越大
     *
     * @return int
     */
    @Override
    public int sort() {
        return 1;
    }
}
