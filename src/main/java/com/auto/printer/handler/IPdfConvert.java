package com.auto.printer.handler;

import com.auto.printer.entity.dto.PdfConvertDTO;

import java.io.IOException;

/**
 * @author LoveHuaHua
 * @date 2023年07月31日 18:31
 * @description believe in yourself
 */
public interface IPdfConvert {

    /**
     * 转换
     *
     * @param fileUrl 文件url
     * @param bytes   字节
     * @return {@link String}
     */
    public PdfConvertDTO convert(String fileUrl, byte[] bytes) throws Exception;

    /**
     * 是否支持
     *
     * @param fileSuffix 文件后缀
     * @return boolean
     */
    public boolean isSupport(String fileSuffix);

    /**
     * 排序 越大优先级越大
     *
     * @return int
     */
    public int sort();
}
