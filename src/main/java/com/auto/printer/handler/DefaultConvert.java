package com.auto.printer.handler;

import com.auto.printer.entity.dto.PdfConvertDTO;
import com.auto.printer.entity.param.QiNiuOssUploadBean;
import com.auto.printer.utils.QiNiuUtils;
import com.auto.printer.utils.WordPdfUtil;
import lombok.RequiredArgsConstructor;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author LoveHuaHua
 * @date 2023年08月01日 8:28
 * @description believe in yourself
 */
@Component
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
public class DefaultConvert implements IPdfConvert {

    private final QiNiuUtils ossUtil;

    /**
     * 转换
     *
     * @param fileUrl 文件地址
     * @param bytes   文件字节
     * @return {@link String}
     */
    @Override
    public PdfConvertDTO convert(String fileUrl, byte[] bytes) throws Exception {
        File file = WordPdfUtil.doc2pdf(fileUrl, bytes);
        QiNiuOssUploadBean qiNiuOssUploadBean = ossUtil.uploadFileByStream(file);
        String url = qiNiuOssUploadBean.getUrl();
        PDDocument load = PDDocument.load(file);
        return new PdfConvertDTO(load, url);
    }

    /**
     * 是否支持
     *
     * @param fileSuffix 文件后缀
     * @return boolean
     */
    @Override
    public boolean isSupport(String fileSuffix) {
        return true;
    }

    /**
     * 排序 越大优先级越大
     *
     * @return int
     */
    @Override
    public int sort() {
        return 0;
    }
}
