package com.auto.printer;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.net.InetAddress;
import java.net.UnknownHostException;


/**
 * @author jie
 */
@Slf4j
@ServletComponentScan
@EnableAsync
@SpringBootApplication
@EnableScheduling
public class PrinterApplication {


    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext run = SpringApplication.run(PrinterApplication.class, args);


        System.out.println("-----------------------------------");
        Environment env = run.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String path = env.getProperty("server.servlet.context-path");
        String envStr = env.getProperty("spring.profiles.active");
        path = StrUtil.isNotEmpty(path) ? path : "";
        log.info("\n----------------------------------------------------------\n\t"
                + "Application  is running! Access URLs:\n\t" + "Local: \t\thttp://localhost:" + port + path
                + "/\n\t" + "External: \thttp://" + ip + ":" + port + path + "/\n\t" + "swagger-ui: \thttp://" + ip
                + ":" + port + path + "/swagger-ui.html\n\t" + "Doc: \t\thttp://" + ip + ":" + port + path
                + "/doc.html\n" + "----------------------------------------------------------");
        envStr = StrUtil.isNotEmpty(envStr) ? envStr : "默认";
        log.info("启动{}配置文件", envStr);
        log.info("启动成功 V0.0.1{}", System.currentTimeMillis());

    }

}
