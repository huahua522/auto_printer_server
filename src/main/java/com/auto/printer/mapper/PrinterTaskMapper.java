package com.auto.printer.mapper;

import com.auto.printer.entity.po.PrinterTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author LoveHuaHua
 * @date 2023年07月25日 10:05
 * @description believe in yourself
 */
@Mapper
public interface PrinterTaskMapper extends BaseMapper<PrinterTask> {
    int deleteByPrimaryKey(Integer id);

    int insert(PrinterTask record);

    int insertSelective(PrinterTask record);

    PrinterTask selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PrinterTask record);

    int updateByPrimaryKey(PrinterTask record);
}