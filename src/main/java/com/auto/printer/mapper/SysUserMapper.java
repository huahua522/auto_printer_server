package com.auto.printer.mapper;

import com.auto.printer.entity.po.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author LoveHuaHua
 * @date 2023年07月25日 10:05
 * @description believe in yourself
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {



}