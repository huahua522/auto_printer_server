package com.auto.printer.common.exception;

public enum ErrorCode {
    UN_KNOW_ERROR(1, "未知错误"),
    COMMON_PARAM_EMPTY(2, "必选参数为空"),
    COMMON_PARAM_ERROR(3, "参数格式错误"),
    USER_VALID_ERROR(4, "用户验证失败"),


    QUERY_RESULT_IS_NULL(5, "查询结果为空"),
    NOT_ONLY_REQUEST(6, "重复请求~"),
    RSA_FAIL(7, "解密错误~~"),

    DATABASE_SAVE_ERROR(8, "保存失败"),
    DATABASE_DELETE_ERROR(9, "删除失败"),

    DATABASE_UPDATE_ERROR(10, "修改失败"),
    DATABASE_ADD_ERROR(11, "增加失败"),
    /**
     * 权限不足
     */
    PERMISSION_ERROR(12, "权限不足~~"),
    IS_NOT_TEACHER(13, "请使用教师身份操作~~"),


    /******************************教学计划***********************/
    /**
     * 修订版本 选择修订前校验
     */
    EDU_PLAN_ID_IS_NULL(-10000, "未选择教学计划"),
    /**
     * 修订版本 输入修订后校验
     */
    EDU_NEW_PLAN_IS_NULL(-10001, "未输入新的教学计划"),

    /**
     * 删除教学计划校验
     */
    EDU_PLAN_IS_USED(-10002, "教学计划已使用,禁止删除"),


    /**
     * 设置班级教学计划 专业下无班级报错
     */
    EDU_MAJOR_NO_CLASS(-10003, "该年级的专业下暂无班级"),

    /**
     * 教学计划版本号重复
     */
    EDU_VERSION_IS_SAME(-10004, "教学计划版本号重复"),

    /**
     * 添加/修改教学计划课程 学时不匹配
     */
    EDU_HOURS_IS_NOT_SAME(-10005, "学时不匹配"),

    /**
     * 学期为必填项
     */
    EDU_YEAR_IS_EMPTY(-10006, "学期为必填项"),
    /**
     * 专业不存在
     */
    MAJOR_NOT_EXIST(-10007, "专业不存在"),

    /**
     * 应该是此专业下同一培养层次已存在培养方案
     */
    CURRENT_YEAR_MAJOR__PLAN_EXIST(-10008, "应该是此专业下同一培养层次已存在培养方案"),
    CURRENT_PLAN_COURSE_EXIST(-10009, "当前教学计划已存在此课程"),
    /**
     * 课程已经提交
     */
    COURSE_HAS_SUBMIT(-10010, "此课程已通过审核,禁止删除"),
    /**
     * 教学计划不存在
     */
    PLAN_NOT_EXIST(-10011, "教学计划不存在"),
    /**
     * 计划已经提交
     */
    PLAN_HAS_SUBMIT(-10012, "教学计划已审核,禁止修改"),

    /**
     * 计划课程不存在
     */
    PLAN_COURSE_NOT_EXIST(-10013, "计划课程不存在"),

    /**
     * 目标专业学年已存在教学计划
     */
    TARGET_MAJOR_YEAR_HAS_PLAN(-10014, "目标专业学年已存在教学计划"),
    /**
     * 已提交教学计划禁止编辑
     */
    HAS_COMMIT_CANT_EDIT(-10015, "已提交教学计划禁止编辑"),
    /**
     * 当前学年专业已有同培养层次的教学计划
     */
    TARGET_MAJOR_YEAR_LENGTH_HAS_PLAN(-10015, "当前学年专业已有同培养层次的教学计划"),
    PLAN_COURSE_STATUS_NOT_AUDIT(-10016, "当前课程未审核通过,禁止微调"),

    SHARD_TEACHER_IS_USED(-10017, "共享教师已使用,禁止删除"),

    COURSE_TEACHER_IS_USED(-10018, "任课教师已使用,禁止删除"),
    PLAN_COURSE_IS_USED(-10019, "此课程已排课,禁止删除"),
    PLAN_COURSE_IS_USED_CAN_RETURN(-10020, "此课程已排课,禁止退审"),


    /******************************教学计划***********************/


    /***************************排课***************************/

    /**
     * 不存在排课学期
     */
    EDU_ARRANGING_NOT_HAVE_SEMESTER(-11000, "当前不存在排课学期"),


    /**
     * 不存在排课班级
     */
    EDU_ARRANGING_NOT_HAVE_CLASS(-11001, "当前年级专业下不存在班级"),

    /**
     * 当前班级不存在教学计划
     */
    EDU_ARRANGING_CLASS_NOT_HAVE_PLAN(-11002, "当前年级专业下不存在班级"),

    /**
     * 该课程暂无任课教师
     */
    EDU_COURSE_NOT_HAVE_TEACHER(-11003, "该课程暂无任课教师"),

    /**
     * 班级无教学计划
     */
    EDU_CLASS_NOT_HAVE_PLAN(-11004, "该班级暂无教学计划"),

    /**
     * 教学计划无此课时
     */
    EDU_PLAN_NOT_HAVE_COURSE(-11005, "该班级的教学计划无此课程"),

    /**
     * 所选时间的无可排课时间
     */
    EDU_TIME_NOT_HAVE_WEEK(-11006, "所选时间的无可排课时间"),

    /**
     * 该班级暂无大课
     */
    EDU_CLASS_NOT_HAVE_BIG_LESSON(-11007, "该班级暂无大课"),

    /**
     * 删除排课信息 字段必传
     */
    EDU_TIME_START_TIME_IS_EMPTY(-11008, "未选择开始时间"),

    /**
     * 不存在当前学期
     */
    EDU_WEEK_NOT_HAVE_SEMESTER(-11009, "不存在该学期"),

    /**
     * 该排课记录不存在
     */
    EDU_TIME_TABLE_NOT_EXIST(-11010, "该排课记录不存在"),
    CHANGE_WEEKS_IS_NOT_EQUALS(-11011, "变更前后周数不一致"),
    TIME_TABLE_CHANGE_IS_NOT_EXIST(-11012, "申请单不存在"),
    TIME_TABLE_CHANGE_HAS_EXECUTE(-11012, "申请单已处理"),
    TEACHER_TIME_NOT_FREE(-11013, "教师时间不满足"),
    ROOM_TIME_NOT_FREE(-11014, "教室时间不满足"),
    MAKEUP_INFO_CANT_EMPTY(-11015, "补课信息不能为空"),
    CHANGE_HAS_SUBMIT(-11016, "申请已提交"),

    WEEK_OVER(-11017, "超时停课总课时"),


    /**
     * 评分
     */
    QUESTION_NOT_ANSWER(-12000, "不存在评分记录"),


    /**
     * 不存在此问卷类型
     */
    QUESTION_NOT_EXIST(-12001, "不存在此问卷类型"),

    /**
     * 已评教
     */
    QUESTION_HAS_ANSWER(-12002, "此问卷已提交"),


    /******************************成绩相关**********************/
    /**
     * 成绩已上传
     */
    SCORE_HAS_UPLOAD(-13000, "成绩已上传,禁止修改"),

    /**
     * 成绩已上传 禁止删除
     */
    SCORE_HAS_UPLOAD_CANT_DELETE(-13001, "成绩已上传,禁止修改"),


    /**
     * 班级已存在
     */
    CLASS_HAS_BIND(-13002, "考试班级已经添加过"),

    /**
     * 班级不存在
     */
    CLASS_NOT_FOUNT(-13003, "班级不存在~"),


    /**********************公选课****************************/
    COURSE_CANT_EMPTY(-14000, "没有学生,不能开课~"),

    COURSE_CANT_D(-14001, "课程名称重复~"),
    REQUEST_CANT_D(-14002, "重复请求~"),
    PARAM_NOT_ALL(-14003, "请先补充选课参数~"),

    /***********************************************/
    COURSE_NOT_FOUND(-14004, "课程不存在~"),

    TIYUKE_HAS(-14005, "当前学期已存在体育课"),

    CLASS_NAME_DISTINCT(-14006, "本学期同名班级已存在~"),
    CAMPUS_NOT_EXIST(-14007, "校区不存在~"),
    EDUCATION_NOT_EXIST(-14008, "学历不存在~"),
    SEX_NOT_EXIST(-14009, "性别不存在~"),
    /**
     * 选课条件不满足
     */
    COURSE_SELECTION_CONDITIONS_ARE_NOT_SATISFIED(-14009, "选课条件不满足~"),
    IM_SORRY_TO_HAVE_BOTH(-14010, "抱歉手慢了~"),
    PLEASE_DO_NOT_REPEAT_COURSE_SELECTION(-14011, "请勿重复选课~"),
    THIS_TYPE_IS_ELECTIVE(-14012, "此类型已选课~"),

    COURSE_SELECTION_TIME_ARE_NOT_SATISFIED(-14013, "选课时间不满足~"),

    PUBLIC_COURSE_FAIL(-14014, "退课失败~"),
    TEACHER_NOT_EXIST(-14015, "教师不存在"),
    START_DATE_NOT_EXIST(-14016, "选课开始日期不能为空"),
    END_DATE_NOT_EXIST(-14017, "选课结束日期不能为空"),
    COURSE_HAS_SURE(-14018, "课程已开课,禁止修改"),


    /**********************节次***********************/
    SECTION_HAS_EXIST(-15000, "节次已存在"),
    /**
     * 开始时间不能早于结束时间
     */
    SECTION_START_TIME_BEFORE_END_TIME(-15001, "开始时间不能早于结束时间"),

    //=============资产处===========//
    REMOVE_CANT_NOT_EMPTY(20001, "不能删除拥有资产的实验室~"),
    IMPORT_ID_NOT_HAVE(20002, "批量修改不能存在系统中没有的资产~"),
    IMPORT_DEPT_NOT_HAVE(20003, "批量修改不能存在无分院的资产~"),
    IMPORT_ROOM_NOT_HAVE(20004, "批量修改不能存在无实验室的资产~"),
    PROPER_NOT_NULL(20005, "资产不能为空~");

    private final int code;
    private final String desc;

    ErrorCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return this.code;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return "ErrorCodeEnum{" + "code='" + code + '\'' + ", desc='" + desc + '\'' + '}';
    }
}
