package com.auto.printer.common.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author LoveHuaHua
 * @date 2022年11月16日 10:10
 * @description believe in yourself
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class BaseColumn implements Serializable {


    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    @OrderBy
    private Integer id;

    /**
     * 创建人
     */
    @TableField(value = "create_by", fill = FieldFill.INSERT)
    private Integer createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    /**
     * 更新人
     */
    @TableField(value = "update_by", fill = FieldFill.INSERT_UPDATE)
    private Integer updateBy;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 删除时间
     */
    @TableLogic
    private LocalDateTime deleteTime;
}
