package com.auto.printer.common.utils.ip;

import cn.hutool.core.net.Ipv4Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author wangjie
 * @date 2022年03月19日 11:09
 * @description 电表工具
 */
@Component
@Slf4j
public class EleUtil {

    /**
     * 阿里云健康检查
     * @param ip
     * @return 是否是阿里云健康检查
     */
    public boolean isTcpExamine(String ip){

        ip = ip.replace("/", "").replace(" ", "");
        String[] split = ip.split(":");
        if(split.length >= 2)
        {
            ip = split[0];
        }

        // 100.64.0.0/10
        long beginIpLong = Ipv4Util.getBeginIpLong("100.64.0.0", 10);

        String endIpStr = Ipv4Util.getEndIpStr("100.64.0.0", 10);

        long endIpLong = Ipv4Util.ipv4ToLong(endIpStr);

        long l = Ipv4Util.ipv4ToLong(ip);

        return (beginIpLong <= l) && (l <= endIpLong);

    }

}
