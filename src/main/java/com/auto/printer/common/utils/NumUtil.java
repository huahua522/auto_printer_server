package com.auto.printer.common.utils;


import cn.hutool.core.util.NumberUtil;

/**
 * @author wangjie
 * @date 2022年12月08日 11:19
 * @description
 */
public class NumUtil {

    public static boolean isNotNan(Number number){
         return  number != null && !NumberUtil.parseNumber("0").equals(number)  && number.intValue() != 0
                 && number.longValue() != 0 ;
    }

}
