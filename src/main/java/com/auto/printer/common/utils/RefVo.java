//package com.auto.printer.common.utils;
//
//import cn.hutool.core.util.ReflectUtil;
//import cn.hutool.core.util.StrUtil;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.google.common.collect.Lists;
//
//import java.lang.reflect.Field;
//import java.util.ArrayList;
//
///**
// * @author wangjie
// * @date 2023年03月17日 14:54
// * @description
// */
//public class RefVo {
//
//    private static String blankStr = " ";
//
//    private static String equalStr = " = ";
//
//    private static String likeSrt = " like '%str%' ";
//
//    private static String str = "'";
//
//
//    public static void refVoSearchParams(Object o,Class c) {
//        ArrayList<String> ignoreField = Lists.newArrayList("serialVersionUID", "searchCount", "page", "size", "total");
//
//        String searchCondition = "searchCondition";
//
//        StringBuilder stringBuilder = new StringBuilder(" 1 = 1 ");
//        if (o != null) {
//                Field[] fields = ReflectUtil.getFields(c);
//                for (Field field : fields) {
//
//                    Class<?> type = field.getType();
//                    String s = field.getName();
//                    //数据库字段
//                    String fieldName = StrUtil.toUnderlineCase(s);
//                    if(ignoreField.contains(s))
//                    {
//                        continue;
//                    }
//
//                    TableField annotation = field.getAnnotation(TableField.class);
//
//                    if(annotation != null)
//                    {
//                        if( !annotation.exist())
//                        {
//                            continue;
//                        }
//                        if(!StrUtil.isBlankIfStr(annotation.value()))
//                        {
//                             fieldName = annotation.value();
//                        }
//                    }
//
//
//
//                    Object fieldValue = ReflectUtil.getFieldValue(o, field.getName());
//
//                    if (fieldValue != null) {
//                        if (type == String.class) {
//                            String v = StrUtil.toString(fieldValue);
//                            if(StrUtil.isBlankIfStr(v))
//                            {
//                                continue;
//                            }
//                            stringBuilder.append(" and ");
//                            stringBuilder.append(fieldName);
//                            String str = likeSrt.replace("str", v.trim());
//                            stringBuilder.append(str);
//                        } else if (fieldValue instanceof Number) {
//                            stringBuilder.append(" and ");
//                            stringBuilder.append(fieldName);
//                            stringBuilder.append(equalStr);
//                            stringBuilder.append(fieldValue);
//                        }
//                    }
//                }
//            ReflectUtil.setFieldValue(o, searchCondition, stringBuilder.toString());
//        }
//    }
//}
