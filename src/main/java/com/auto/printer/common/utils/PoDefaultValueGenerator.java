//package com.auto.printer.common.utils;
//
//
//import org.springframework.beans.BeanUtils;
//
//import java.lang.reflect.Method;
//import java.time.LocalDateTime;
//import java.util.Date;
//
///**
// * @author wangjie
// * @date 2022年03月29日 10:29
// * @description 默认值注入
// */
//public class PoDefaultValueGenerator {
//    public static final String CB = "createBy";
//    public static final String CD = "createTime";
//    public static final String UB = "updateBy";
//    public static final String UD = "updateTime";
//
//
//    public PoDefaultValueGenerator() {
//    }
//
//    public static <T> void putDefaultValue(T t, String name, Object value) {
//        try {
//            Method getMethod = BeanUtils.findMethod(t.getClass(), "get" + name.substring(0, 1).toUpperCase() + name.substring(1), new Class[0]);
//            if (getMethod == null) {
//                return;
//            }
//
//            Class returnType = getMethod.getReturnType();
//            Method setMethod;
//            if (returnType == Date.class) {
//                setMethod = BeanUtils.findMethod(t.getClass(), "set" + name.substring(0, 1).toUpperCase() + name.substring(1), new Class[]{returnType});
//                if (setMethod == null) {
//                    return;
//                }
//                setMethod.invoke(t, new Date());
//            }
//
//            if (returnType == LocalDateTime.class) {
//                setMethod = BeanUtils.findMethod(t.getClass(), "set" + name.substring(0, 1).toUpperCase() + name.substring(1), new Class[]{returnType});
//                if (setMethod == null) {
//                    return;
//                }
//
//                setMethod.invoke(t, LocalDateTime.now());
//            }
//
//            if (value != null && returnType == value.getClass()) {
//                setMethod = BeanUtils.findMethod(t.getClass(), "set" + name.substring(0, 1).toUpperCase() + name.substring(1), new Class[]{returnType});
//                if (setMethod == null) {
//                    return;
//                }
//
//                setMethod.invoke(t, value);
//            }
//        } catch (Exception var6) {
//            var6.printStackTrace();
//        }
//
//    }
//
//    public static <T> void putCreateValue(T t) {
//
//        putCreateValue(t, true);
//
//    }
//
//    /**
//     *
//     * @param t 要注入的对象
//     * @param login 是否强制登入
//     * @param <T>
//     */
//    public static <T> void putCreateValue(T t, boolean login) {
//
//        putDefaultValue(t, CD, null);
//        if (login) {
//            putDefaultValue(t, CB, UserHolder.getId());
//        } else {
//            putDefaultValue(t, CB, UserHolder.getLocalIfLogin().getId());
//        }
//    }
//
//
//    public static <T> void putUpdateValue(T t) {
//
//        putDefaultValue(t, UD, null);
//
//        putDefaultValue(t, UB, UserHolder.getId());
//
//    }
//
//}
