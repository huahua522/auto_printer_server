package com.auto.printer.common.utils;


import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author jie
 */
@Component
public class RedisDefaultUtils {


    @Autowired
    private RedisTemplate<String, String> redisTemplate;


    /**
     * @param key
     * @return
     */
    public String getValue(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    //

    /**
     * 设置缓存
     *
     * @param key
     * @param value
     */
    public void setValue(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 设置缓存,过期时间,单位 秒
     */
    public void setValue(String key, String value, long s) {
        redisTemplate.opsForValue().set(key, value, s, TimeUnit.SECONDS);
    }

    @Async
    public void setSynValue(String key, String value, long s) {
        redisTemplate.opsForValue().set(key, value, s, TimeUnit.SECONDS);
    }

    /**
     * 判断缓存是否存在
     *
     * @param key
     * @return
     */
    public boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 设置过期时间
     *
     * @param key
     * @param s
     * @return
     */
    public boolean expire(String key, long s) {
        return redisTemplate.expire(key, s, TimeUnit.SECONDS);
    }

    /**
     * 删除缓存
     *
     * @param key
     * @return
     */
    public boolean delete(String key) {
        if (redisTemplate.hasKey(key)) {
            return redisTemplate.delete(key);
        }

        return false;
    }

    /**
     * 通过increment(K key, long delta)方法以增量方式存储long值（正值则自增，负值则自减）
     *
     * @param key
     * @param increment
     * @return
     */
    public Long addBySelf(String key, long increment) {
        return redisTemplate.opsForValue().increment(key, increment);
    }


    public void setHash(String key, String hKey, String hValue) {
        this.redisTemplate.opsForHash().put(key, hKey, hValue);
    }


    public void setAllHash(String key, HashMap<String, String> hKey) {
        this.redisTemplate.opsForHash().putAll(key, hKey);
    }

    public Map<Object, Object> getAllHash(String key) {
        return this.redisTemplate.opsForHash().entries(key);
    }

    public void deleteHash(String key, String hKey) {
        this.redisTemplate.opsForHash().delete(key, hKey);
    }

    public String getHash(String key, String hKey) {
        Object o = this.redisTemplate.opsForHash().get(key, hKey);
        return StrUtil.toString(o);
    }


    public void setList(String k, String v) {
        this.redisTemplate.opsForList().leftPush(k, v);
    }

    public void setList(String k, String v, long s) {
        this.redisTemplate.opsForList().leftPush(k, v);
        this.expire(k, s);
    }


    public void setList(String k, List<String> v) {
        this.redisTemplate.opsForList().leftPushAll(k, v);


    }

    public void setList(String k, List<String> v, long s) {
        this.redisTemplate.opsForList().leftPushAll(k, v);

        this.expire(k, s);
    }


    public List<String> getList(String k, Integer number) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            String s = this.redisTemplate.opsForList().rightPop(k);
            if (!StrUtil.isBlankIfStr(s)) {
                list.add(s);
            }
        }
        return list;
    }

    public List<String> getList(String k) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            String s = this.redisTemplate.opsForList().rightPop(k);
            if (!StrUtil.isBlankIfStr(s)) {
                list.add(s);
            }

        }
        return list;
    }
}
