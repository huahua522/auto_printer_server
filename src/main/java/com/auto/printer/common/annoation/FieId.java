package com.auto.printer.common.annoation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 列表接口返回字段注解
 * @author jie
 */

@Target({ElementType.FIELD,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface FieId {

    /**
     * 字段是否显示
     * @return
     */
    boolean value() default true;

    /**
     * 字段名
     * @return
     */
    String name() default "";

    /**
     * 字段描述
     * @return
     */
    String msg() default "";


    /**
     * 排序
     * @return
     */
    int number() default 9999;

    /**
     * 数据类型  优先级高于 JsonFormat
     * @return
     */
    String pattern() default "";

    /**
     * 字典
     * @return
     */
    String dictKey() default "";

    /**
     * "{'1':'本科','2':'专科'}"
     * 手动配置字典  配置低于dictKey(会被字典值覆盖，因为改字典表更方便),  {key:value,key:value}
     * @return
     */
    String dictJson() default "";

    /**
     * 字段宽带
     * @return
     */
    String width() default "";



}
