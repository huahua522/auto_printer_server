package com.auto.printer.common.annoation;

import com.auto.printer.common.utils.RedisDefaultUtils;
import com.auto.printer.common.utils.RedisUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wangjie
 * @date 2023年03月03日 8:42
 * @description 启用
 */
@Configuration
public class UtilsConfig {

    @Bean
    @ConditionalOnMissingBean({RedisLockAspect.class})
    public RedisLockAspect redisLockAspect() {
        return new RedisLockAspect();
    }



    @Bean
    @ConditionalOnMissingBean({RedisUtils.class})
    public RedisUtils redisUtils() {
        return new RedisUtils();
    }

    @Bean
    @ConditionalOnMissingBean({RedisDefaultUtils.class})
    public RedisDefaultUtils redisDefaultUtils() {
        return new RedisDefaultUtils();
    }

    @Bean
    @ConditionalOnMissingBean({CheckParamAspect.class})
    public CheckParamAspect checkParamAspect() {
        return new CheckParamAspect();
    }
}
