package com.auto.printer.common.annoation;

import java.lang.annotation.*;

/**
 * 分布式锁
 * @author jie
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RedisBusinesLock {
    /**
     * 业务前缀
     * @return
     */
    String prefix() default "lock";

    /**
     * 业务方法标识
     * 默认锁方法名
     * @return
     */
    String busines() default "";

    /**
     * 业务方法key标识
     * 默认锁方法名
     * @return
     */
    String key() default "";

    /**
     *锁住提示
     * @return
     */
    String msg() default "正在操作,请稍后再试";

    /**
     * 锁存时间（秒值） 默认锁定30s
     * @return
     */
    long leaseTime() default 30L;

    /**
     * 允许等待时间
     * @return
     */
    long waitTime() default 0L;

    /**
     *是否打印错误日志
     * @return
     */
    boolean showLog() default false;
}
