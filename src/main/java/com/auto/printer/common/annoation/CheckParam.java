package com.auto.printer.common.annoation;

import java.lang.annotation.*;

/**
 * @author jie
 *校验参数
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckParam {


    /**
     * 校验规则
     * {"#searchKey != ''","#sysDictData != null and #sysDictData.id != null"}
     * @return 规则
     */
    String[] check() default {};

    /**
     * 校验未通过提示
     * @return 校验未通过提示
     */
    String[] msg() default {};
}

