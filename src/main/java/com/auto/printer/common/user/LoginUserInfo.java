//package com.auto.printer.common.user;
//
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;
//import lombok.Data;
//
//import java.util.List;
//
//
///**
// * 登入用户信息
// * @author jie 通过接口获取的
// */
//@ApiModel(value = "登入用户信息")
//@Data
//public class LoginUserInfo {
//
//    @ApiModelProperty(value = "uid")
//    private Integer id;
//
//    @ApiModelProperty(value = "姓名")
//    private String name;
//
//    @ApiModelProperty(value = "1:学生 2:教师 3:商户 4:管理员 5测试")
//    private Integer identity;
//
//    @ApiModelProperty(value = "是否有所有数据权限")
//    private Boolean fullDataPermission;
//
//    @ApiModelProperty(value = "访问时的token")
//    private String token;
//
//    @ApiModelProperty(value = "角色")
//    private List<Role> roles;
//
//    @ApiModelProperty(value = "用户编码，适配OA,格式为：姓名-uid")
//    private String userCode;
//
//
//    @Data
//    public static class Role {
//        private Integer id;
//        private Integer role_id;
//        private Integer user_id;
//        private String role_name;
//    }
//
//    public String getUserCode() {
//        return this.name+"-"+this.id;
//    }
//}