//package com.auto.printer.common.user;
//
//
//import com.gl.common.exception.MyException;
//
///**
// * 全局用户信息持有，通过鉴权拦截器操作
// * @author jie
// */
//public class UserHolder {
//    private static ThreadLocal<LoginUserInfo> LOCAL = new ThreadLocal<LoginUserInfo>();
//
//    private UserHolder() {
//    }
//
//    public static LoginUserInfo getLocal() {
//        LoginUserInfo loginUserInfo = LOCAL.get();
//        if(loginUserInfo == null)
//        {
//            throw new MyException("非法访问,请先登入");
//        }
//        return loginUserInfo;
//    }
//
//    /**
//     * 可能返回空
//     * @return
//     */
//    public static LoginUserInfo getLocalIfLogin() {
//        return LOCAL.get() == null ? new LoginUserInfo() : LOCAL.get() ;
//    }
//    public static Integer getId() {
//        return getLocal().getId();
//    }
//
//
//    public static void setLocal(LoginUserInfo local) {
//
//        UserHolder.LOCAL.set(local);
//
//    }
//
//    public static void remove() {
//        LOCAL.remove();
//    }
//}
