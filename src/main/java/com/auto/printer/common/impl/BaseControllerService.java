package com.auto.printer.common.impl;

import java.util.Map;

/**
 * @author wangjie
 * @date 2023年04月11日 10:15
 * @description 接口返回字典渲染字典 强制 实现此接口
 */
public interface BaseControllerService {


    /**
     * 字典数据
     * @param dictKey
     * @return
     */
    Map<String,String> getSysDictData(String dictKey);


    /**
     * 渲染列表OSS地址 待开发
     * @return
     */
    String getOssFileUrl();
}
