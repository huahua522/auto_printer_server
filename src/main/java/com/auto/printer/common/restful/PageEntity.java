package com.auto.printer.common.restful;


import com.alibaba.excel.annotation.ExcelIgnore;
import com.auto.printer.common.annoation.FieId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
//import com.gl.common.annoation.FieId;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wangjie
 * @date 2022年02月17日 18:41
 * @description
 */
@Data
public class PageEntity<T> implements Serializable {
    /**
     * 总数
     */
//    @ApiModelProperty("总数")
    @TableField(exist = false)
    @FieId(value = false)
    @JsonProperty(access = Access.WRITE_ONLY)
    @ExcelIgnore
    protected long total = 0;
    /**
     * 每页显示条数，默认 10
     */

//    @ApiModelProperty("每页显示条数，默认 10")
    @TableField(exist = false)
    @FieId(value = false)
    @JsonProperty(access = Access.WRITE_ONLY)
    @ExcelIgnore
    protected long size = 10;

    /**
     * 当前页
     */
//    @ApiModelProperty("当前页")
    @TableField(exist = false)
    @FieId(value = false)
    @JsonProperty(access = Access.WRITE_ONLY)
    @ExcelIgnore
    protected long page = 1;


    /**
     * mybatis-plus 自动查总数标识
     */
//    @ApiModelProperty("mybatis-plus 自动查总数标识")
    @TableField(exist = false)
    @FieId(value = false)
    @JsonIgnore
    @ExcelIgnore
    protected boolean searchCount = true;


    @FieId(value = false)
    @JsonIgnore
    public <E> Page<E> getPageObj() {
        return new Page<>(page, size,total,searchCount);
    }
    @FieId(value = false)
    public <E> Page<E> getPageObj(long p, long s) {
        return new Page<>(p,s,searchCount);
    }
    @FieId(value = false)
    public <E> Page<E> getPageObj(long p, long s, boolean searchCount) {
        return new Page<>(p,s,searchCount);
    }



}
