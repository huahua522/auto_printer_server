package com.auto.printer.common.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.interceptor.CacheErrorHandler;

/**
 * @author wangjie
 * @date 2021年08月23日 16:51
 * @description 忽略redis缓存失败导致业务无法正常运行
 */
@Slf4j
public class IgnoreExceptionCacheErrorHandler implements CacheErrorHandler {

    @Override
    public void handleCacheGetError(RuntimeException exception, Cache cache, Object key) {
        //log.error(exception.getMessage(), exception);
        log.error("redis错误1");
    }

    @Override
    public void handleCachePutError(RuntimeException exception, Cache cache, Object key, Object value) {
        //log.error(exception.getMessage(), exception);
        log.error("redis错误2");
    }

    @Override
    public void handleCacheEvictError(RuntimeException exception, Cache cache, Object key) {
        //log.error(exception.getMessage(), exception);
        log.error("redis错误3");
    }

    @Override
    public void handleCacheClearError(RuntimeException exception, Cache cache) {
        //log.error(exception.getMessage(), exception);
        log.error("redis错误4");
    }

}
