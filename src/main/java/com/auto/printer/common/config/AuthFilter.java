//package com.auto.printer.common.config;
//
//import cn.hutool.core.collection.CollUtil;
//import cn.hutool.core.date.DateUtil;
//import cn.hutool.core.util.StrUtil;
//import cn.hutool.http.HttpUtil;
//import cn.hutool.json.JSONUtil;
//import com.gl.common.restful.ResultModel;
//import com.gl.common.user.LoginUserInfo;
//import com.gl.common.user.UserHolder;
//import com.gl.common.utils.RedisUtils;
//import com.gl.jxue.application.dict.service.SysDictDataService;
//import com.gl.jxue.common.utils.REDIS;
//import com.google.common.collect.Lists;
//import lombok.Data;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//
//import javax.servlet.*;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//
///**
// * @author wangjie
// * @date 2022年07月11日 11:18
// * @description java接口鉴权
// */
//
//@Slf4j
//@WebFilter(filterName = "AuthFilter", urlPatterns = {"/*"})
//public class AuthFilter implements Filter {
//    @Autowired
//    private RedisUtils redisUtils;
//    @Autowired
//    private SysDictDataService sysDictDataService;
//
//    @Value("${auth.uri}")
//    private String authUri;
//
//    @Value("${auth.open}")
//    private boolean authOpen;
//
//    @Value("${server.servlet.context-path}")
//    private String contextPath;
//
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//        log.info("拦截器初始化 "+ DateUtil.formatDateTime(new Date()));
//
//
//
//        System.out.println("                    _ooOoo_                         \n" +
//                "                   o8888888o                        \n" +
//                "                   88\" . \"88                        \n" +
//                "                   (| -_- |)                        \n" +
//                "                   O\\  =  /O                        \n" +
//                "                ____/'---'\\____                     \n" +
//                "              .  \\\\|       |//  .                   \n" +
//                "             /  |||||  :  |||||  \\                  \n" +
//                "            /  -||||| -:- |||||-  \\                 \n" +
//                "            |   | \\\\\\  -  /// |   |                 \n" +
//                "            |   |  ''\\---/''  |   |                 \n" +
//                "            \\  .-\\__  `-`  ___/-. /                 \n" +
//                "          ___`. .' / --.-- \\ '. .`  ___                \n" +
//                "       .\"\" <  `.___\\_ <|> _/___.' > \"\".             \n" +
//                "      | | :  `- \\`.;`\\ _ /`;.`/ - ` : | |           \n" +
//                "      \\  \\ `-.   \\_ __\\ /__ _/   .-` /  /           \n" +
//                " ======`-.____`-.___\\_____/___.-`____.-'======      \n" +
//                "                    '=---='                         \n" +
//                "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^       \n" +
//                "            佛祖保佑       永不宕机 ");
//    }
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        List<String> ignoreUri =
//                Lists.newArrayList(
//                        "/face/faceDeviceTask/runTimeTask",
//                        "person/personDetailPapers/submitTeacherPapers",
//                        "/stuAffairs/stuAffairsCertificateType/",
//                        "/v1/wx/getWxArticleList",
//                        "/v1/wx/wxGetUnionId",
//
//                        "/authorize/userAuthorize/exportTemplate",
//                        "/authorize/userAuthorize/exportTemplate",
//                        "/authorize/userAuthorize/getInfoByUserPhone",
//                        "/oss/sysOssFile/uploadHeadFile",
//                        "/sys/personTeacher/registerTeacher",
//                        "/v1/sysDictData/getDictByType",
//                        "/sys/personDepartment/getDeptList",
//                        "/authorization/requestApi/",
//                        "/oss/sysOssFile/oaFile",
//                        "/stuReDeplTask/uploadFile",
//                        //考勤打卡
//                        "/clockController/clockLister",
//                        "/edu/CoursesArranging/generateSpecialClass",
//                        //考勤日统计
//                        "/clockReportController/allCockInfo"
//                );
//
//        try {
//            HttpServletRequest request = (HttpServletRequest) servletRequest;
//            String uri = request.getRequestURI().replaceAll("//","/");
//            String token = request.getHeader("token");
//            String client = request.getHeader("client");
//            String method = request.getMethod();
//            log.info("请求地址{},token:{},method:{}", uri,token,method);
//            //如果是配置文件未开启 获取是跨域检查请求 正常放行
//            if ("OPTIONS".equals(method)) {
//                filterChain.doFilter(servletRequest,servletResponse);
//                return;
//            }
//
//
//            Map<String, String> javaIgnoreUrl = sysDictDataService.getSysDictDataMap("java_ignore_url");
//
//            Map<String, String> runHttpService = sysDictDataService.getSysDictDataMap("run_http_service");
//
//
//            if(CollUtil.isNotEmpty(javaIgnoreUrl))
//            {
//                ignoreUri.addAll(javaIgnoreUrl.values());
//            }
//            if (runHttpService != null) {
//                ignoreUri.addAll(runHttpService.values());
//            }
//
//
//            List<String> collect = ignoreUri.stream().map(f -> {
//
//                return contextPath + f;
//
//            }).map(f->{
//                f = f.replaceAll("//","/");
//                if(f.startsWith("/"))
//                {
//                    return f;
//                }else {
//                    return "/"+f;
//                }
//            }).collect(Collectors.toList());
//
//            ignoreUri.addAll(collect);
//
//            String o = redisUtils.getValue(REDIS.SYS_USERS_TOKEN + token+uri);
//            if (!StrUtil.isBlankIfStr(o) && JSONUtil.isTypeJSON(o)) {
//                log.info("/***********用户缓存{}*************/", o);
//                LoginUserInfo loginUserInfo = JSONUtil.toBean(o, LoginUserInfo.class);
//                UserHolder.setLocal(loginUserInfo);
//                filterChain.doFilter(servletRequest, servletResponse);
//                return;
//            }
//
//            /**
//             * 忽略地址也要去鉴权，但不是每次去鉴权
//             */
//            if(redisUtils.hasKey(REDIS.SYS_IGNORE_URL + token+uri))
//            {
//                filterChain.doFilter(servletRequest, servletResponse);
//                return;
//            }
//            //其余情况 进行鉴权
//            ResultModel<AuthData> auth = auth(uri, token, method, client);
//            if (auth.getCode() == 200) {
//
//                LoginUserInfo loginUserInfo = new LoginUserInfo();
//                loginUserInfo.setId(auth.getData().getId());
//                loginUserInfo.setName(auth.getData().getName());
//                loginUserInfo.setIdentity(auth.getData().getIdentity());
//                loginUserInfo.setToken(token);
//                UserHolder.setLocal(loginUserInfo);
//                redisUtils.setValue(REDIS.SYS_USERS_TOKEN + token+uri, JSONUtil.toJsonStr(loginUserInfo), 300);
//                filterChain.doFilter(servletRequest, servletResponse);
//                return;
//            }else {
//                log.error("鉴权未通过{}",JSONUtil.toJsonStr(auth));
//            }
//
//            if (!authOpen) {
//                log.info("未开启强制鉴权{}",uri);
//
//                redisUtils.setValue(REDIS.SYS_IGNORE_URL + token+uri, "未开启强制鉴权", 60);
//
//                filterChain.doFilter(servletRequest, servletResponse);
//
//                return;
//            }else {
//                //如果是忽略url 放行
//                for (String s : ignoreUri) {
//                    if (StrUtil.isNotEmpty(s) && StrUtil.isNotEmpty(uri) && uri.contains(s)) {
//                        log.info("忽略地址{}",uri);
//                        redisUtils.setValue(REDIS.SYS_IGNORE_URL + token+uri, "忽略地址", 60);
//                        filterChain.doFilter(servletRequest, servletResponse);
//                        return;
//                    }
//                }
//            }
//
//            HttpServletResponse response = (HttpServletResponse) servletResponse;
//            response.setHeader("Access-Control-Allow-Origin", "*");
//            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
//            response.setHeader("Access-Control-Max-Age", "3600");
//            response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
//            response.setHeader("Access-Control-Expose-Headers", "content-disposition");
//            response.setCharacterEncoding("utf-8");
//            response.getWriter().write(JSONUtil.toJsonStr(auth));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            ResultModel<String> model = new ResultModel<>();
//
//            model.setMessage(StrUtil.toString(e.getMessage()));
//            model.setData(StrUtil.toString(e.getCause()));
//            model.setCode(500);
//
//            HttpServletResponse response = (HttpServletResponse) servletResponse;
//
//            response.setHeader("Access-Control-Allow-Origin", "*");
//            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
//            response.setHeader("Access-Control-Max-Age", "3600");
//            response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
//            response.setHeader("Access-Control-Expose-Headers", "content-disposition");
//            response.setCharacterEncoding("utf-8");
//            response.getWriter().println(JSONUtil.toJsonStr(model));
//
//        }finally {
//            UserHolder.remove();
//        }
//    }
//
//    @Override
//    public void destroy() {
//        UserHolder.remove();
//        log.error("拦截器退出");
//    }
//
//    private ResultModel<AuthData> auth(String uri, String token, String method, String client) {
//        long start = System.currentTimeMillis();
//        String errorMessage = "鉴权异常";
//        try {
//
//            if (StrUtil.isBlankIfStr(token)) {
//                ResultModel<AuthData> authDat = new ResultModel<>();
//                authDat.setMessage("token为空");
//                authDat.setCode(500);
//                return authDat;
//            }
//            Map<String, Object> hashMap = new HashMap<>();
//            hashMap.put("uri", uri);
//            hashMap.put("method", method);
//            String jsonStr = JSONUtil.toJsonStr(hashMap);
//            log.info(jsonStr);
//            log.info("token {}", token);
//            String post = HttpUtil.createPost(authUri).header("token", StrUtil.isBlankIfStr(token) ? "" : token).header("client", client)
//                    .body(jsonStr).execute().body();
//            log.info("post 响应:{}", post);
//            long end = System.currentTimeMillis();
//            log.info("鉴权耗时 {}",end-start);
//            if(JSONUtil.isTypeJSON(post))
//            {
//                ResultModel resultModel = JSONUtil.toBean(post, ResultModel.class);
//                ResultModel<AuthData> authDat = new ResultModel<>();
//                BeanUtils.copyProperties(resultModel, authDat);
//                if (resultModel.getCode() == 200) {
//                    Object rows = resultModel.getData();
//                    AuthData authData = JSONUtil.toBean(JSONUtil.toJsonStr(rows), AuthData.class);
//                    BeanUtils.copyProperties(resultModel, authDat);
//                    authDat.setData(authData);
//                    return authDat;
//                }
//                log.info("/*****鉴权结果{}******/", authDat.toString());
//                return authDat;
//            }
//            errorMessage+=post;
//        }catch (Exception e){
//            e.printStackTrace();
//            errorMessage += e.getMessage();
//        }
//
//        return new ResultModel<AuthData>(404,errorMessage,0L,Lists.newArrayList(),null);
//    }
//
//    @Data
//     static
//    class AuthData {
//        /**
//         * UID
//         */
//        private Integer id;
//        /**
//         *
//         */
//        private String name;
//        /**
//         * 身份
//         */
//        private Integer identity;
//    }
//}
//
//
//
//
