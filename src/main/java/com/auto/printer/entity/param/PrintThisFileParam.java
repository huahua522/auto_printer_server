package com.auto.printer.entity.param;

import lombok.Data;

/**
 * @author LoveHuaHua
 * @date 2023年07月05日 19:15
 * @description believe in yourself
 */
@Data
public class PrintThisFileParam {
    /**
     * 文件下载地址
     */
    private String fileUrl;
}
