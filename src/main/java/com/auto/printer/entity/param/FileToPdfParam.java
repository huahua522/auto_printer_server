package com.auto.printer.entity.param;

import lombok.Data;

/**
 * 文件转url
 * @author LoveHuaHua
 * @date 2023年07月05日 20:08
 * @description believe in yourself
 */
@Data
public class FileToPdfParam {

    /**
     * 文件url
     */
    private String fileUrl;
}
