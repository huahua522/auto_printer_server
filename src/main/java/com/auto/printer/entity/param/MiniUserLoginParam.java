package com.auto.printer.entity.param;

import lombok.Data;

/**
 * @author LoveHuaHua
 * @date 2023年07月25日 8:43
 * @description believe in yourself
 */
@Data
public class MiniUserLoginParam {

    /**
     * 微信的code
     */
    private String code;
}
