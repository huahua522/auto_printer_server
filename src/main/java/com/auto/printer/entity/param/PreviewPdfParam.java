package com.auto.printer.entity.param;

import lombok.Data;

/**
 * 预览Pdf
 * @author LoveHuaHua
 * @date 2023年07月14日 20:07
 * @description believe in yourself
 */
@Data
public class PreviewPdfParam {
    /**
     * url
     */
    private String fileUrl;
    /**
     * 页码
     */
    private String pages;
}
