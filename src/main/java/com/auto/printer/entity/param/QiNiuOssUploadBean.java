package com.auto.printer.entity.param;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author LoveHuahua
 * @date 2021年08月21日 9:16
 * @description believe in yourself
 */
@NoArgsConstructor
@Data
public class QiNiuOssUploadBean {


    @JsonProperty("key")
    private String key;
    @JsonProperty("hash")
    private String hash;
    @JsonProperty("bucket")
    private String bucket;
    @JsonProperty("fsize")
    private Integer fsize;

    @JsonProperty("url")
    private String url;
}
