package com.auto.printer.entity.param;

import lombok.Data;

/**
 * @author LoveHuaHua
 * @date 2023年07月28日 13:09
 * @description believe in yourself
 */
@Data
public class GetPdfInfoParam {
    /**
     * 文件url
     */
    private String fileUrl;
}
