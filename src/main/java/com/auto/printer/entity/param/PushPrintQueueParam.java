package com.auto.printer.entity.param;

import lombok.Data;

/**
 * @author LoveHuaHua
 * @date 2023年08月14日 20:09
 * @description believe in yourself
 */
@Data
public class PushPrintQueueParam {

    /**
     * 文件url
     */
    private String fileUrl;

    /**
     * 份数
     */
    private Integer count;

    /**
     * 页面数
     */
    private String pages;


    /**
     * 是否双页打印
     */
    private Integer sideFlag;

    /**
     * 是否彩色打印
     */
    private Integer colorFlag;



}
