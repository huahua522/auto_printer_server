package com.auto.printer.entity.vo;

import com.lowagie.text.pdf.PRTokeniser;
import lombok.Data;

/**
 * @author LoveHuaHua
 * @date 2023年07月28日 13:11
 * @description believe in yourself
 */
@Data
public class PdfInfoVo {

    /**
     * url
     */
    private String url;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 页面数
     */
    private Integer pageCount;


}
