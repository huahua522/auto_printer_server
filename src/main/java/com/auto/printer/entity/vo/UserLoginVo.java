package com.auto.printer.entity.vo;

import lombok.Data;

/**
 * @author LoveHuaHua
 * @date 2023年07月25日 10:13
 * @description believe in yourself
 */
@Data
public class UserLoginVo {

    /**
     * 令牌
     */
    private String token;

    /**
     * 错误信息
     */
    private String message;

    /**
     * 是否登录成功 0=否 1=是
     */
    private Integer flag;
}
