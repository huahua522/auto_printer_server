package com.auto.printer.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.pdfbox.pdmodel.PDDocument;

/**
 * @author LoveHuaHua
 * @date 2023年08月13日 11:59
 * @description believe in yourself
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PdfConvertDTO {

    /**
     * pd文档
     */
    private PDDocument pdDocument;

    /**
     * url
     */
    private String url;
}
