package com.auto.printer.entity.po;

import com.auto.printer.common.entity.BaseColumn;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * @author LoveHuaHua
 * @date 2023年07月25日 10:05
 * @description believe in yourself
 */
@Data
@TableName(value = "printer_task")
public class PrinterTask extends BaseColumn {


    /**
     * 任务编号
     */
    @TableField(value = "task_code")
    private String taskCode;

    /**
     * 打印开始时间
     */
    @TableField(value = "start_time")
    private LocalDateTime startTime;

    /**
     * 打印结束时间
     */
    @TableField(value = "end_time")
    private LocalDateTime endTime;

    /**
     * 文件
     */
    @TableField(value = "file_path")
    private String filePath;


    /**
     * 打印机编号
     */
    @TableField(value = "printer_no")
    private String printerNo;

    /**
     * 是否完成
     */
    @TableField(value = "task_status")
    private Integer taskStatus;

    /**
     * 是否双页
     */
    @TableField(value = "side_flag")
    private Integer sideFlag;

    /**
     * 是否彩色
     */
    @TableField(value = "color_flag")
    private Integer colorFlag;

    /**
     * 错误消息
     */
    @TableField(value = "error_message")
    private String errorMessage;
}