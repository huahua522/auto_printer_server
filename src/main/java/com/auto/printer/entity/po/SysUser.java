package com.auto.printer.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * @author LoveHuaHua
 * @date 2023年07月25日 10:05
 * @description believe in yourself
 */
@Data
@TableName(value = "sys_user")
public class SysUser {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "mini_open_id")
    private String miniOpenId;
    /**
     * 昵称
     */
    @TableField(value = "nick_name")
    private String nickName;

    /**
     * 头像
     */
    @TableField(value = "avatar")
    private String avatar;

    /**
     * 是否有效 0=否 1=是
     */
    @TableField(value = "`status`")
    private Integer status;

}