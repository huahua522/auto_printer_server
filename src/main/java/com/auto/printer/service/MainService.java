package com.auto.printer.service;

import com.auto.printer.entity.param.FileToPdfParam;
import com.auto.printer.entity.param.GetPdfInfoParam;
import com.auto.printer.entity.param.PreviewPdfParam;
import com.auto.printer.entity.param.PushPrintQueueParam;
import com.auto.printer.entity.vo.PdfInfoVo;

import java.io.IOException;
import java.util.List;

/**
 * @author LoveHuaHua
 * @date 2023年07月14日 20:08
 * @description believe in yourself
 */
public interface MainService {
    /**
     * 文件转pdf
     *
     * @param param 参数
     * @return {@link String}
     */
    String fileToPdf(FileToPdfParam param) throws Exception;

    /**
     * pdf预览
     *
     * @param param 参数
     * @return {@link String}
     */
    String previewPdf(PreviewPdfParam param) throws IOException;

    /**
     * 用户用户的文件->pdf信息(此方法包含文件转换)
     *
     * @param param 参数
     * @return {@link PdfInfoVo}
     */
    PdfInfoVo getPdfInfo(GetPdfInfoParam param) throws Exception;

    /**
     * 推送打印队列
     *
     * @param param 参数
     */
    void pushPrintQueue(List<PushPrintQueueParam> param) throws Exception;
}
