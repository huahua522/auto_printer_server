package com.auto.printer.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.http.HttpUtil;
import com.auto.printer.entity.dto.PdfConvertDTO;
import com.auto.printer.entity.param.*;
import com.auto.printer.entity.po.PrinterTask;
import com.auto.printer.entity.vo.PdfInfoVo;
import com.auto.printer.enums.PrintTaskStatusEnum;
import com.auto.printer.handler.PdfConvertManage;
import com.auto.printer.service.MainService;
import com.auto.printer.service.PrinterTaskService;
import com.auto.printer.utils.PDFPageUtil;
import com.auto.printer.utils.QiNiuUtils;
import com.auto.printer.utils.TaskCodeUtil;
import lombok.RequiredArgsConstructor;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author LoveHuaHua
 * @date 2023年07月14日 20:08
 * @description believe in yourself
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
@Transactional(rollbackFor = Exception.class)
public class MainServiceImpl implements MainService {
    private final QiNiuUtils ossUtil;
    private final PdfConvertManage pdfConvertManage;
    private final PrinterTaskService printerTaskService;


    /**
     * 文件转pdf
     *
     * @param param 参数
     * @return {@link String}
     */
    @Override
    public String fileToPdf(FileToPdfParam param) throws Exception {
        PdfConvertDTO convert = pdfConvertManage.convert(param.getFileUrl());
        File newFile = new File(FileUtil.getPrefix(param.getFileUrl()) + "处理后.PDF");
        //保存到本地
        convert.getPdDocument().save(newFile);
        QiNiuOssUploadBean qiNiuOssUploadBean = ossUtil.uploadFileByStream(newFile);
        return qiNiuOssUploadBean.getUrl();
    }

    /**
     * pdf预览
     *
     * @param param 参数
     * @return {@link String}
     */
    @Override
    public String previewPdf(PreviewPdfParam param) throws IOException {
        File downloaded = HttpUtil.downloadFileFromUrl(param.getFileUrl(), "download/" + UUID.fastUUID() + "/" + FileUtil.getName(param.getFileUrl()));
        //
        PDDocument load = PDDocument.load(downloaded);
        PDFPageUtil.executePdf(param.getPages(), load);
        File newFile = new File(FileUtil.getPrefix(downloaded) + "处理后.PDF");
        //保存到本地
        load.save(newFile);
        QiNiuOssUploadBean qiNiuOssUploadBean = ossUtil.uploadFileByStream(newFile);
        return qiNiuOssUploadBean.getUrl();
    }

    /**
     * 用户用户的文件->pdf信息(此方法包含文件转换)
     *
     * @param param 参数
     * @return {@link PdfInfoVo}
     */
    @Override
    public PdfInfoVo getPdfInfo(GetPdfInfoParam param) throws Exception {
        PdfConvertDTO convert = pdfConvertManage.convert(param.getFileUrl());
        PDDocument pdDocument = convert.getPdDocument();
        PdfInfoVo pdfInfoVo = new PdfInfoVo();
        pdfInfoVo.setUrl(convert.getUrl());
        pdfInfoVo.setPageCount(pdDocument.getNumberOfPages());
        pdfInfoVo.setFileName(FileUtil.getName(convert.getUrl()));
        return pdfInfoVo;
    }

    /**
     * 推送打印队列
     *
     * @param param 参数
     */
    @Override
    public void pushPrintQueue(List<PushPrintQueueParam> param) throws Exception {
        for (PushPrintQueueParam pushPrintQueueParam : param) {
            String fileUrl = pushPrintQueueParam.getFileUrl();
            PdfConvertDTO convert = pdfConvertManage.convert(fileUrl);

            PDDocument pdDocument = convert.getPdDocument();
            PDFPageUtil.executePdf(pushPrintQueueParam.getPages(), pdDocument);
            File newFile = new File(FileUtil.getPrefix(fileUrl) + "打印版本.PDF");
            //保存到本地
            pdDocument.save(newFile);
            QiNiuOssUploadBean qiNiuOssUploadBean = ossUtil.uploadFileByStream(newFile);
            //处理后.pdf
            String url = qiNiuOssUploadBean.getUrl();
            PrinterTask entity = new PrinterTask();
            entity.setTaskStatus(PrintTaskStatusEnum.QUEUE.getCode());
            entity.setFilePath(url);
            entity.setTaskCode(TaskCodeUtil.generateCode());
            //默认设备
            entity.setPrinterNo("HP Smart Tank 750 series");
            entity.setColorFlag(pushPrintQueueParam.getColorFlag());
            entity.setSideFlag(pushPrintQueueParam.getSideFlag());
            printerTaskService.save(entity);
        }
    }
}
