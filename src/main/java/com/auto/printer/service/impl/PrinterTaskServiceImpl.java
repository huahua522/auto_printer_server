package com.auto.printer.service.impl;

import com.auto.printer.service.PrinterTaskService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.auto.printer.entity.po.PrinterTask;
import com.auto.printer.mapper.PrinterTaskMapper;

/**
 * @author LoveHuaHua
 * @date 2023年07月25日 10:05
 * @description believe in yourself
 */
@Service
public class PrinterTaskServiceImpl extends ServiceImpl<PrinterTaskMapper, PrinterTask> implements PrinterTaskService {

}
