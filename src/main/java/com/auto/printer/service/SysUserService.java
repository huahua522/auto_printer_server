package com.auto.printer.service;

import com.auto.printer.entity.param.MiniUserLoginParam;
import com.auto.printer.entity.po.SysUser;
import com.auto.printer.entity.vo.UserLoginVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author LoveHuaHua
 * @date 2023年07月25日 10:05
 * @description believe in yourself
 */
public interface SysUserService extends IService<SysUser> {


    /**
     * 微信小程序登录
     *
     * @param miniUserLoginParam 迷你用户登录参数
     * @return {@link String}
     */
    UserLoginVo loginByMini(MiniUserLoginParam miniUserLoginParam);

}
