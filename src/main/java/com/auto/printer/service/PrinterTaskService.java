package com.auto.printer.service;

import com.auto.printer.entity.po.PrinterTask;
import com.baomidou.mybatisplus.extension.service.IService;
    /**
 * @author LoveHuaHua
 * @date 2023年07月25日 10:05
 * @description believe in yourself
 */
public interface PrinterTaskService extends IService<PrinterTask>{


}
