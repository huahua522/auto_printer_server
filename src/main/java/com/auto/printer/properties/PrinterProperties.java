package com.auto.printer.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author LoveHuaHua
 * @date 2023年08月15日 14:26
 * @description believe in yourself
 */
@Data
@Component
@ConfigurationProperties(prefix = "printer")
public class PrinterProperties {

    private List<String> deviceList;
}
