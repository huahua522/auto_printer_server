package com.auto.printer.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.io.unit.DataUnit;
import cn.hutool.core.util.RandomUtil;

/**
 * @author LoveHuaHua
 * @date 2023年08月14日 20:25
 * @description believe in yourself
 */
public class TaskCodeUtil {

    public static final String CODE_PREFIX = "花花_";
    public static final Integer CODE_LEN = 18;

    public static String generateCode() {
        return CODE_PREFIX + DateUtil.format(LocalDateTimeUtil.now(),"yyyyMMdd") +  RandomUtil.randomNumbers(CODE_LEN);
    }

}

