package com.auto.printer.utils;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WxUtil {
    private final static String APP_ID = "wx65ad4abb069c88a7";
    private final static String APP_SECRET = "40d53989f16eb575ece4b7446f88db4c";
    //
    private final static String WX_LOGIN_SERVER_URL = "https://api.weixin.qq.com/sns/jscode2session?appid={}&secret={}&js_code={}&grant_type=authorization_code";

    public static String getOpenId(String code) {
        String url = StrUtil.format(WX_LOGIN_SERVER_URL, APP_ID, APP_SECRET, code);
        String response = HttpUtil.get(url);
        log.info("请求微信响应:{}",response);
        return JSONUtil.parseObj(response).getStr("openid");
    }
}