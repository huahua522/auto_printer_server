package com.auto.printer.utils;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import com.auto.printer.common.exception.ErrorCode;
import com.auto.printer.common.exception.MyException;

import java.util.Collection;

/**
 * @author LoveHuahua
 * @date 2022年03月26日 16:02
 * @description believe in yourself
 */
public class CheckUtil {

    /**
     * 如果是空 那么抛出异常
     *
     * @param collection 集合
     * @param errorCode 错误码
     */
    public static void checkNotEmpty(Collection<?> collection, ErrorCode errorCode) {
        if (CollUtil.isEmpty(collection)) {
            throw new MyException(errorCode);
        }
    }

    /**
     * 如果是空 那么抛出异常
     *
     * @param collection 集合
     * @param exception 异常
     */
    public static void checkNotEmpty(Collection<?> collection,RuntimeException exception) {
        if (CollUtil.isEmpty(collection)) {
            throw exception;
        }
    }


    /**
     * 如果是空 那么抛出异常
     *
     * @param object 对象
     * @param errorCode 错误码
     */
    public static void checkNotNull(Object object, ErrorCode errorCode) {
        if (object == null) {
            throw new MyException(errorCode);
        }
    }

    public static void checkNotNull(Object object, RuntimeException exception) {
        checkTrue(ObjUtil.isNotNull(object), exception);
    }

    /**
     * 检测是否为true
     *
     * @param condition 条件
     * @param errorCode 错误代码
     */
    public static void checkTrue(boolean condition, ErrorCode errorCode) {
        if (!condition) {
            throw new MyException(errorCode);
        }
    }


    /**
     * 检测str 不是默认
     *
     * @param exception 异常
     * @param str       str
     */
    public static void checkStrNotBlank(String str, RuntimeException exception) {
        checkTrue(StrUtil.isNotBlank(str),exception);
    }

    public static void checkTrue(Boolean condition, RuntimeException exception) {
        if (!BooleanUtil.isTrue(condition)) {
            throw exception;
        }
    }

    public static void checkFalse(boolean condition, ErrorCode errorCode) {
        checkTrue(!condition, errorCode);
    }

    public static void checkFalse(boolean condition,  RuntimeException exception) {
        checkTrue(!condition, exception);
    }


}
