package com.auto.printer.controller;

import cn.hutool.core.lang.UUID;
import cn.hutool.http.HttpUtil;
import com.auto.printer.common.restful.BaseController;
import com.auto.printer.entity.param.*;
import com.auto.printer.entity.vo.PdfInfoVo;
import com.auto.printer.service.MainService;
import com.auto.printer.utils.Printer;
import com.auto.printer.utils.QiNiuUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.util.List;

/**
 * @author LoveHuaHua
 * @date 2023年07月05日 19:14
 * @description believe in yourself
 */
@RestController
@RequestMapping("/print")
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
public class MainPrintController extends BaseController {

    private final  QiNiuUtils ossUtil;
    private final MainService mainService;
    /**
     * 打印机名字
     */
    private String printName = "F5B1CC";

    /**
     * 获取七牛云上传 token
     *
     * @return {@link Object}
     */
    @GetMapping("/getUpToken")
    public Object getUpToken(){
     return toResult(ossUtil.getUpToken());
    }

//    /**
//     * 打印
//     *
//     * @return {@link Object}
//     */
//    @PostMapping("/printThisFile")
//    public Object print(@RequestBody PrintThisFileParam param) throws Exception {
//        File downloaded = HttpUtil.downloadFileFromUrl(param.getFileUrl(), "download/" + UUID.fastUUID() + "/");
//        Printer.handlerFileDoc(downloaded, printName);
//        return toResult();
//    }


    /**
     * 文件转换为pdf
     *
     * @return {@link Object}
     */
    @PostMapping("/fileToPdf")
    public Object fileToPdf(@RequestBody FileToPdfParam param) throws Exception {
        String url = mainService.fileToPdf(param);
        return toResult(url);
    }

    /**
     * 得到pdf格式信息
     *
     * @return {@link Object}
     */
    @PostMapping("/getPdfInfo")
    public Object getPdfInfo(@RequestBody GetPdfInfoParam param) throws Exception {
        PdfInfoVo pdfInfoVo = mainService.getPdfInfo(param);
        return toResult(pdfInfoVo);
    }

    /**
     * pdf预览
     *
     * @param param 参数
     * @return {@link Object}
     */
    @PostMapping("/previewPdf")
    public Object previewPdf(@RequestBody PreviewPdfParam param) throws IOException {
        String url = mainService.previewPdf(param);
        return toResult(url);
    }

    /**
     * 上传打印队列
     *
     * @return {@link Object}
     */
    @PostMapping("/pushPrintQueue")
    public Object pushPrintQueue(@RequestBody List<PushPrintQueueParam> param) throws Exception {
        mainService.pushPrintQueue(param);
        return toResult();
    }

}
