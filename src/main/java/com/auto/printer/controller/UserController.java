package com.auto.printer.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import com.auto.printer.common.restful.BaseController;
import com.auto.printer.entity.param.MiniUserLoginParam;
import com.auto.printer.entity.po.SysUser;
import com.auto.printer.entity.vo.UserLoginVo;
import com.auto.printer.service.SysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

/**
 * @author LoveHuaHua
 * @date 2023年07月25日 8:39
 * @description believe in yourself
 */
@RequestMapping("/user")
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
public class UserController extends BaseController {

    private final SysUserService sysUserService;

    /**
     * 登录(根据code换取openID)
     *
     * @return {@link Object}
     */
    @PostMapping("/loginByMini")
    public Object loginByMini(@RequestBody MiniUserLoginParam miniUserLoginParam) {
        SysUser byId = sysUserService.getById(2);
        String token2 = JWTUtil.createToken(BeanUtil.beanToMap(byId), "huahua_printer".getBytes());
        UserLoginVo token = sysUserService.loginByMini(miniUserLoginParam);
        return toResult(token);
    }



    @PostMapping("/getUserInfo")
    public Object getUserInfo(@RequestHeader("Authorization") String authorization){
        String token = authorization.replaceAll("Bearer ", "");
        JWT jwt = JWTUtil.parseToken(token);
        return toResult(jwt.getPayloads());
    }
}
