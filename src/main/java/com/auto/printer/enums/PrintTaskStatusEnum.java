package com.auto.printer.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author LoveHuaHua
 * @date 2023年08月14日 20:48
 * @description believe in yourself
 */
@Getter
@RequiredArgsConstructor
public enum PrintTaskStatusEnum {

    QUEUE(0,"排队中"),
    ING(1,"打印中"),
    SUCCESS(2,"成功"),
    FAIL(3,"失败")
    ;


    private final int code;
    private final String message;


}
